# Wulf's Frontend

A starting point for Wulf's projects(Front End).

- [Sass 3.3](http://sass-lang.com/)
- [autoprefixer](https://github.com/ai/autoprefixer) for vendor prefixes
- [metaquery](https://github.com/benschwarz/metaquery) for responsive breakpoints
- [SMACSS](http://smacss.com/)-style modules with [BEM](http://bem.info/method/) syntax
- A [Susy 2](http://susy.oddbird.net/) mobile-first responsive grid module
- [Normalize.css](http://necolas.github.com/normalize.css/)
- [Hologram](http://trulia.github.io/hologram/)
- [Guard Hologram](https://github.com/kmayer/guard-hologram)


## Getting started

Please follow each step:

### Standalone compilation

Requires ruby, node.js and [bundler](http://bundler.io/): `gem install bundler`

Install dependencies:

- `bundle install`
- `npm install`
- `bower install`

Then run `make` or `make watch` to compile CSS into `css/`


### Use with your asset pipeline

Just drop the `stylesheets` directory into your usual stylesheets path (note the dependencies in `Gemfile`, `package.json` & `bower.json`).


### Hologram - quick start

`hologram init`
=> This will create `hologram_config.yml`, `_header.html`, & `_footer.html` files. Tweak the config as needed. 
If you see 'Warning: Cowardly refusing to overwrite existing hologram_config.yml', remove the existing `hologram_config.yml` & tweak the source on the config.

On the doc_assets/_header.html, update "<link rel="stylesheet" href="./your_stylesheet_here.css">"

If you want to run Hologram manually, you can exclude `guard-hologram` and run `hologram` instead. For more in depth information, go to [Hologram's git page](http://trulia.github.io/hologram/)

### Guard hologram
After installation for Guard hologram is finished, you can type `guard init` on terminal & to start the watch, type `bundle exec guard`

#### Sample Guardfile
```Guardfile
guard "hologram", config_path: "hologram.yml" do
  watch(%r{app/assets/stylesheets/.*css})
  watch(%r{app/views/docs/styleguide/doc_assets/.+})
  watch('hologram.yml')
end
```



## Modules ~ Information from [Ben Smithett](http://bensmithett.com/)

Modules are the core of Style's architecture. A module:

- Is defined in its own file (eg `modules/_my_module.sass`)
- Is isolated, reusable & disposable.
- Has no knowledge of its context (i.e. doesn't depend on styles from a particular parent element - it can be rendered anywhere)
- Minimises its own [depth of applicability](http://smacss.com/book/applicability) so that it can safely contain other modules
- Has no context-specific dimensions. Read [Objects in Space](https://medium.com/objects-in-space/f6f404727) for more on this.

### Simple module

Here's what a simple module, `/stylesheets/modules/_simple_widget.sass`, might look like:

```sass
.simple-widget
  color: goldenrod
```

### Complex module

Here's a slightly more complex module, `/stylesheets/modules/_comment.sass`:
```sass
.comment
  color: fuchsia

  // State is applied with a second class...
  &.is-loading
    background: url(spinner.gif)

  // ...or with a data attribute
  &[data-state=loading]
    background: url(spinner.gif)

// A modified comment
.comment--important
  @extend .comment
  font-weight: bold

// A submodule (some module that *must* be a child of .comment)
// Whatever is inside a submodule can usually be extracted out into its own module.
// In this case, .comment__avatar is a container for a separate .avatar module.
.comment__avatar
  margin-left: 20px
  width: 100px
```

## Media queries
Media queries in CSS are for chumps. [Use metaquery](http://glenmaddern.com/metaquery-and-the-end-of-media-queries/) for mobile-first responsive modules:

```sass
.my-module
  color: floralwhite
  
  .breakpoint-tablet &
    color: plum
  
  .breakpoint-desktop &
    color: burlywood
```

## Grid
Style comes with a `.grid` module. It's just a [Susy](http://susydocs.oddbird.net/) container. When you put modules inside a `.grid`, you can use Susy's functions & mixins to align your module to the grid.

There are a lot, but the main one you'll use is [`span`](http://susydocs.oddbird.net/en/latest/toolkit/#span-mixin):

```sass
.page__sidebar
  +span(3 of 12)

.page__content
  +span(last 9 of 12)
```

See the included `.page` module for a responsive example.

## License
Style is released under the [MIT License](http://ben.mit-license.org/)
